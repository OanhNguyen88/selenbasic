package TestCases;

import Data.AccountsData;
import Objects.Products;
import Pages.CartPage;
import Pages.LoginPage;
import Pages.ProductPage;
import Utils.Constants;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;

public class TestCartPage extends BaseTest {
    ProductPage productPage;
    LoginPage loginPage;
    CartPage cartPage;
    ArrayList<Products> getSelectedItemInfoFromProductPage;

    @BeforeClass
    @Parameters({"browserName"})
    public void SetUp(@Optional("Chrome") String browserName) {
        super.SetUp(browserName);
        loginPage = new LoginPage(driver);
        cartPage = new CartPage(driver);
        productPage = new ProductPage(driver);
        loginPage.get(Constants.URL);
        loginPage.login(AccountsData.defaultAccount());
        getSelectedItemInfoFromProductPage = productPage.getSelectedItemInfoFromProductPage();
        productPage.selectItemFromProductPage();
        productPage.clickIcon();

    }

    @AfterClass
    public void TearDown() {
        super.TearDown();
    }


    @Test (groups = {"smoketest", "regressiontest"}, priority = 1,
            description = "This is a product test case ")
    public void verifyItemInfo() {
        ArrayList<Products> itemInfoInCart = cartPage.getItemInfoInCart();

        for (int i = 0; i < Constants.SELECTED_PRODCT_NUMBER_DEFAULT(); i++) {
            Assert.assertEquals(itemInfoInCart.get(i).getProductName(),
                    getSelectedItemInfoFromProductPage.get(i).getProductName());
            Assert.assertEquals(itemInfoInCart.get(i).getProductDesc(),
                    getSelectedItemInfoFromProductPage.get(i).getProductDesc());
            Assert.assertEquals(itemInfoInCart.get(i).getProductPrice(),
                    getSelectedItemInfoFromProductPage.get(i).getProductPrice());


        }
    }



}
