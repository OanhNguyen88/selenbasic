package Locators;

import org.openqa.selenium.By;

public class CheckoutStepTwoPageLocators {
    //ellements of paymentInformation, shippingInformation, itemTotal, tax, total, cancelButton, finishButton
    public static By item_name (int i) {
        return By.xpath("//div[@class='cart_item'][" + i +
                "]//div[@class='inventory_item_name']");
    }

    public static By item_desc (int i) {
        return By.xpath("//div[@class='cart_item'][" + i +
                "]//div[@class='inventory_item_desc']");
    }

    public static By item_price (int i) {
        return By.xpath("//div[@class='cart_item'][" + i +
                "]//div[@class='inventory_item_price'][text()[2]]");
    }

    public static By finish_button = By.xpath("//*[@id='finish']");

    public static By item_total = By.xpath("//*[@class = 'summary_subtotal_label'][text()[2]]");
}
