package Pages;

import Locators.CartPageLocators;
import Locators.CheckoutStepOnePageLocators;
import Locators.CheckoutStepTwoPageLocators;
import Objects.Products;
import Utils.Constants;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class CheckOutStep2Page extends BasePage {
    public CheckOutStep2Page(WebDriver driver) {
        super(driver);
    }

    public ArrayList<Products> getItemInfo () {
        ArrayList<Products> getItemInfo = new ArrayList<>();
        for (int i = 1; i <= Constants.SELECTED_PRODCT_NUMBER_DEFAULT(); i++) {
            Products products = getAProductInfo(CheckoutStepTwoPageLocators.item_name(Constants.PRODUCT_INDEX[i - 1]),
                    CheckoutStepTwoPageLocators.item_desc(Constants.PRODUCT_INDEX[i - 1]),
                    CheckoutStepTwoPageLocators.item_price(Constants.PRODUCT_INDEX[i - 1]));
            getItemInfo.add(products);
        }
        return getItemInfo;
    }

    public float compareTotalProductPrice() {
        float totalProductPrice = 0;
        float ItemTotal;



        return totalProductPrice - ItemTotal;

    }

    public float calculateTotalProductPrice() {
        for (int i; i <= Constants.SELECTED_PRODCT_NUMBER_DEFAULT(); i++) {
            totalProductPrice += Float.parseFloat(findElement(CheckoutStepTwoPageLocators.
                    item_price(Constants.PRODUCT_INDEX[i - 1])).getText());
        }

    }

    public void getItemTotal() {
        ItemTotal = Float.parseFloat(findElement(CheckoutStepTwoPageLocators.item_total).getText());
    }

}
