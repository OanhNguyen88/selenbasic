package Locators;

import org.openqa.selenium.By;

public class CheckoutStepOnePageLocators {
    public static By input_firstName = By.id("first-name");
    public static By input_lastName = By.id("last-name");
    public static By input_postalCode = By.id("postal-code");
    public static By continue_button = By.id("continue");
    public static By cancel_button = By.id("cancel");

}
