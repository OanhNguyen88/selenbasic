package TestCases;

import Data.AccountsData;
import Locators.ProductPageLocators;
import Pages.LoginPage;
import Pages.ProductPage;
import Utils.Constants;
import org.testng.Assert;
import org.testng.annotations.*;


public class TestProductPage extends BaseTest {
    ProductPage productPage;
    LoginPage loginPage;


    @BeforeClass
    @Parameters({"browserName"})
    public void SetUp(@Optional("Chrome") String browserName) {
        super.SetUp(browserName);
        loginPage = new LoginPage(driver);
        loginPage.get(Constants.URL);

        loginPage.login(AccountsData.defaultAccount());

        productPage = new ProductPage(driver);


    }

    @AfterClass
    public void TearDown() {
        super.TearDown();
    }


    @Test(groups = {"smoketest", "regressiontest"}, priority = 1,
            description = "This is a product test case 01")
    public void verifyAddButton() {
        productPage.selectItemFromProductPage();
        Assert.assertEquals(Integer.parseInt(productPage.getText(ProductPageLocators.cartIcon)),
                Constants.SELECTED_PRODCT_NUMBER_DEFAULT());

    }
//    @Test (groups = {"smoketest", "regressiontest"}, priority = 1,
//            description = "This is a product test case 01")
//    public void verifyRemoveButton() {
//        verifyAddButton();
//
//        for (int i = 1; i <= Constants.SELECTED_PRODCT_NUMBER_DEFAULT(); i++) {
//            productPage.click(ProductPageLocators.product_RemoveFromCart(i));
//        }
//
//        Assert.assertTrue(productPage.getText(ProductPageLocators.cartIcon).isEmpty());
//
//    }
}


