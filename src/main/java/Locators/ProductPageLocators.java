package Locators;

import org.openqa.selenium.By;

public class ProductPageLocators {
    public static By product_list = By.xpath("//div[@class='inventory_item']");

    public static By product_name (int i) {
        return By.xpath("//div[@class='inventory_item'][" + i + "]//div[@class='inventory_item_name']");
    }

    public static By product_desc (int i) {
        return By.xpath("//div[@class='inventory_item'][" + i + "]//div[@class='inventory_item_desc']");
    }

    public static By product_price (int i) {
        return By.xpath("//div[@class='inventory_item'][" + i + "]//div[@class='inventory_item_price']");
    }

    public static By product_addToCart (int i) {
        return By.xpath("//div[@class='inventory_item'][" + i + "]//*[text()='Add to cart']");
    }

    public static By product_RemoveFromCart (int i) {
        return By.xpath("//div[@class='inventory_item'][" + i + "]//button[text()='Remove'");
    }

    public static By cartIcon = By.xpath("//*[@id='shopping_cart_container']");


}