package Utils;

public class Constants {
    public static String URL = "https://www.saucedemo.com/";
    public static String LOGIN_URL = "https://www.saucedemo.com/inventory.html";
    public static String PRODUCT_URL = "https://www.saucedemo.com/inventory.html";
    public static String CART_URL = "https://www.saucedemo.com/cart.html";
    public static String CHECKOUT1_URL = "https://www.saucedemo.com/checkout-step-one.html";
    public static String CHECKOUT1_FIRSTNAME = "OANH";
    public static String CHECKOUT1_LASTNAME = "NGUYEN";
    public static String CHECKOUT1_POSTALCODE = "1188";
    public static String CHECKOUT2_URL = "https://www.saucedemo.com/checkout-step-two.html";
    public static String CHECKOUT3_URL = "https://www.saucedemo.com/checkout-step-one.html";
    public static String CHECKOUTCOMPLETE_URL = "https://www.saucedemo.com/checkout-complete.html";
    public static String USERNAME = "standard_user";
    public static String PASSWORD = "secret_sauce";
    public static Integer[] PRODUCT_INDEX = {1, 2, 3};
    public static int SELECTED_PRODCT_NUMBER_DEFAULT () {
        return PRODUCT_INDEX.length;
    }
    public static int TIMEOUT = 30; //seconds
    public static String ACCOUNT_CSV_FILE = "src/main/resources/accounts.csv";
    public static String PRODUCT_CSV_FILE = "src/main/java/Objects/Products.java";


}
