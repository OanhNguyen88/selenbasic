 package TestCases;

import Data.AccountsData;
import Objects.Products;
import Pages.CartPage;
import Pages.CheckOutStep1Page;
import Pages.LoginPage;
import Pages.ProductPage;
import Utils.Constants;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;

public class TestCheckOutStep1Page extends BaseTest {
    ProductPage productPage;
    LoginPage loginPage;
    CartPage cartPage;
    CheckOutStep1Page checkOutStep1Page;
    ArrayList<Products> getSelectedItemInfoFromProductPage;

    @BeforeClass
    @Parameters({"browserName"})
    public void SetUp(@Optional("Chrome") String browserName) {
        super.SetUp(browserName);
        loginPage = new LoginPage(driver);
        cartPage = new CartPage(driver);
        productPage = new ProductPage(driver);
        checkOutStep1Page = new CheckOutStep1Page(driver);
        loginPage.get(Constants.URL);
        loginPage.login(AccountsData.defaultAccount());
        productPage.selectItemFromProductPage();
        productPage.clickIcon();
        cartPage.clickCheckOutButton();

    }

    @AfterClass
    public void TearDown() {
        super.TearDown();
    }


    @Test (groups = {"smoketest", "regressiontest"}, priority = 1,
            description = "This is a product test case ")
    public void verifyCompleteYourInfoForm() {
        checkOutStep1Page.completeCheckOutStepOne();
        Assert.assertTrue(driver.getCurrentUrl().equals(Constants.CHECKOUT2_URL));

    }

}
