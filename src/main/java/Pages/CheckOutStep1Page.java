package Pages;

import Locators.CartPageLocators;
import Locators.CheckoutStepOnePageLocators;
import Objects.Products;
import Utils.Constants;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class CheckOutStep1Page extends BasePage {
    public CheckOutStep1Page(WebDriver driver) {
        super(driver);
    }

    public void completeCheckOutStepOne () {
        sendKeys(CheckoutStepOnePageLocators.input_firstName, Constants.CHECKOUT1_FIRSTNAME);
        sendKeys(CheckoutStepOnePageLocators.input_lastName, Constants.CHECKOUT1_LASTNAME);
        sendKeys(CheckoutStepOnePageLocators.input_postalCode, Constants.CHECKOUT1_POSTALCODE);
        click(CheckoutStepOnePageLocators.continue_button);
    }
}
