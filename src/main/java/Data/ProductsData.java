package Data;

import Objects.Accounts;
import Objects.Products;
import Utils.Constants;
import Utils.Utility;

import java.util.ArrayList;

public class ProductsData {
    public static ArrayList<Products> getProductCSV(String file, String delimiter) {
        ArrayList<String> arrayList = Utility.readCSV(file);
        ArrayList<Products> productsListFromData = new ArrayList<>();

        for (Object string : arrayList) {
            String[] array = string.toString().split(delimiter);
            productsListFromData.add(new Products(array[0], array[1], array[2]));
        }

        return productsListFromData;
    }
}
