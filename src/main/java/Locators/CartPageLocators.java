package Locators;

import org.openqa.selenium.By;

public class CartPageLocators {
    public static By continue_Shopping_Button = By.id("continue-shopping");

    public static By checkout_Button = By.id("checkout");

    public static By item_name (int i) {
        return By.xpath("//div[@class='cart_item'][" + i +
                "]//div[@class='inventory_item_name']");
    }

    public static By item_desc (int i) {
        return By.xpath("//div[@class='cart_item'][" + i +
                "]//div[@class='inventory_item_desc']");
    }

    public static By item_price (int i) {
        return By.xpath("//div[@class='cart_item'][" + i +
                "]//div[@class='inventory_item_price']");
    }


}
