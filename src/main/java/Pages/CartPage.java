package Pages;

import Locators.CartPageLocators;
import Locators.ProductPageLocators;
import Objects.Products;
import Utils.Constants;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class CartPage extends BasePage {
    public CartPage(WebDriver driver) {
        super(driver);
    }

    public ArrayList<Products> getItemInfoInCart() {
        ArrayList<Products> getSelectedItemInCart = new ArrayList<>();
        for (int i = 1; i <= Constants.SELECTED_PRODCT_NUMBER_DEFAULT(); i++) {
            Products products = getAProductInfo(CartPageLocators.item_name(Constants.PRODUCT_INDEX[i - 1]),
                    CartPageLocators.item_desc(Constants.PRODUCT_INDEX[i - 1]),
                    CartPageLocators.item_price(Constants.PRODUCT_INDEX[i - 1]));
            getSelectedItemInCart.add(products);
        }
        return getSelectedItemInCart;
    }

    public void clickCheckOutButton () {
        click(CartPageLocators.checkout_Button);
    }
}
