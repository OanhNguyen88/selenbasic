 package TestCases;

import Data.AccountsData;
import Objects.Products;
import Pages.*;
import Utils.Constants;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;

 public class TestCheckOutStep2Page extends BaseTest {
     ProductPage productPage;
     LoginPage loginPage;
     CartPage cartPage;
     CheckOutStep1Page checkOutStep1Page;
     CheckOutStep2Page checkOutStep2Page;
     ArrayList<Products> getSelectedItemInfoFromProductPage;

     @BeforeClass
     @Parameters({"browserName"})
     public void SetUp(@Optional("Chrome") String browserName) {
         super.SetUp(browserName);
         loginPage = new LoginPage(driver);
         cartPage = new CartPage(driver);
         productPage = new ProductPage(driver);
         checkOutStep1Page = new CheckOutStep1Page(driver);
         checkOutStep2Page = new CheckOutStep2Page(driver);
         loginPage.get(Constants.URL);
         loginPage.login(AccountsData.defaultAccount());
         getSelectedItemInfoFromProductPage = productPage.getSelectedItemInfoFromProductPage();
         productPage.selectItemFromProductPage();
         productPage.clickIcon();
         cartPage.clickCheckOutButton();
         checkOutStep1Page.completeCheckOutStepOne();

     }

     @AfterClass
     public void TearDown() {
         super.TearDown();
     }


     @Test (groups = {"smoketest", "regressiontest"}, priority = 1,
             description = "This is a product test case ")
     public void verifyItemInfo() {
         ArrayList<Products> getItemInfo = checkOutStep2Page.getItemInfo();

         for (int i = 0; i < Constants.SELECTED_PRODCT_NUMBER_DEFAULT(); i++) {
             Assert.assertEquals(getItemInfo.get(i).getProductName(),
                     getSelectedItemInfoFromProductPage.get(i).getProductName());
             Assert.assertEquals(getItemInfo.get(i).getProductDesc(),
                     getSelectedItemInfoFromProductPage.get(i).getProductDesc());
             Assert.assertEquals(getItemInfo.get(i).getProductPrice(),
                     getSelectedItemInfoFromProductPage.get(i).getProductPrice());
         }
     }

     @Test
     public void verifyTotalPriceBeforeTax () {
         Assert.assertTrue(checkOutStep2Page.compareTotalProductPrice() = 0);
     }
 }
