package Pages;

import Locators.ProductPageLocators;
import Objects.Products;
import Utils.Constants;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class ProductPage extends BasePage{
    public ProductPage(WebDriver driver) {
        super(driver);
    }

//    public int listSize = getProductQuantity(ProductPageLocators.product_list);


    public ArrayList<Products> getAllProductInfoFromApp(int listSize) {
        ArrayList<Products> productListFromApp = new ArrayList<>();

        int i = 0;

        while (i <= listSize) {
            Products products = getAProductInfo(ProductPageLocators.product_name(i),
                    ProductPageLocators.product_desc(i),ProductPageLocators.product_price(i));
            productListFromApp.add(products);
            i++;
        }
        return productListFromApp;
    }

    public void selectItemFromProductPage() {
        for (int i = 1; i <= Constants.SELECTED_PRODCT_NUMBER_DEFAULT(); i++) {
            click(ProductPageLocators.product_addToCart(Constants.PRODUCT_INDEX[i - 1]));

        }
    }

    public void clickIcon () {
        click(ProductPageLocators.cartIcon);
    }

    public ArrayList<Products> getSelectedItemInfoFromProductPage() {
        ArrayList<Products> getSelectedItemInfoFromProductPage = new ArrayList<Products>();
        for (int i = 1; i <= Constants.SELECTED_PRODCT_NUMBER_DEFAULT(); i++) {
            Products products = getAProductInfo(ProductPageLocators.product_name(Constants.PRODUCT_INDEX[i - 1]),
                    ProductPageLocators.product_desc(Constants.PRODUCT_INDEX[i - 1]),
                    ProductPageLocators.product_price(Constants.PRODUCT_INDEX[i - 1]));
            getSelectedItemInfoFromProductPage.add(products);
        }
        return getSelectedItemInfoFromProductPage;
    }

}


