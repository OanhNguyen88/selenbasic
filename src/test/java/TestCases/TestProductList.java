package TestCases;

import Data.AccountsData;
import Locators.ProductPageLocators;
import Objects.Products;
import Pages.LoginPage;
import Pages.ProductPage;
import Utils.Constants;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;

public class TestProductList {
    public class TestLogin extends BaseTest {
        ProductPage productPage;
        LoginPage loginPage;


        @BeforeClass
        @Parameters({"browserName"})
        public void SetUp(@Optional("Chrome") String browserName) {
            super.SetUp(browserName);
            productPage = new ProductPage(driver);
            loginPage.login(AccountsData.defaultAccount());

        }

        @AfterClass
        public void TearDown() {
            super.TearDown();
        }


        @Test(groups = {"smoketest", "regressiontest"}, priority = 1,
                description = "This is a login test case 01")
        public void verifyProductInfoList() {
            String delimiter = ";";
            int listSize = productPage.getProductQuantity(ProductPageLocators.product_list);
            ArrayList<Products> productInfoFromApp = productPage.
                    getAllProductInfoFromApp(listSize);
            ArrayList<Products> productIfoFromData = Data.ProductsData.getProductCSV(Constants.PRODUCT_CSV_FILE, delimiter);

//            productPage.get(Constants.PRODUCT_URL);

            for (int i = 0; i < listSize; i++) {
                Assert.assertEquals(productInfoFromApp.get(i).getProductName(), productIfoFromData.get(i).getProductName());
                Assert.assertEquals(productInfoFromApp.get(i).getProductDesc(), productIfoFromData.get(i).getProductDesc());
                Assert.assertEquals(productInfoFromApp.get(i).getProductPrice(), productIfoFromData.get(i).getProductPrice());
            }
        }
    }
}
